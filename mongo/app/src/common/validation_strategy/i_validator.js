/*
 @i_validator.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const CompilersServiceError = require('./../errors/compilers_service_error');


// builds IValidator class (simulating an interface)
class IValidator {

    // Abstract method for its children that returns a validation
    validate() {
        throw new CompilersServiceError("Not implemented");
    }
}

// Exports IValidator class
module.exports = IValidator;
