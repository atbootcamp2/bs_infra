/*
@command_factory.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const JavaCommand = require("./java_command");
const PythonCommand = require("./python_command");
const NodeCommand = require("./node_command");
const CSharpCommand = require("./csharp_command");


class CommandFactory {
    constructor() {
        this.command_map = new Map();
        this.load_command();
    }

    get_command(language) {
        return this.command_map.get(language);
    }

    load_command() {
        this.command_map.set('java', new JavaCommand ());
        this.command_map.set('python', new PythonCommand ());
        this.command_map.set('node', new NodeCommand ());
        this.command_map.set('csharp', new CSharpCommand ());
    }
}

module.exports = CommandFactory;
