/*
 @utils_project_controller_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


var expect = require("chai").expect;
const ValidationProjectController = require('../../../common/utils/validation_project_controller');
const CompilersServiceError = require('../../../common/errors/compilers_service_error');


describe('validation project controller test', () => {

    // Negative tests from validate_save_file_data()
    it("Don't send language as parameter in validate_save_file_data()", () => {
        let validation_file_controller = new ValidationProjectController();
        expect(() => { validation_file_controller.validate_save_project('E:\Git\compilers_service\projects', 'pruebaJava'); }).to.throw(CompilersServiceError, 'Language in null or empty');
    });
    
    it("Don't send language and project_name and project_id as parameters in validate_save_file_data()", () => {
        let validation_file_controller = new ValidationProjectController();
        expect(() => { validation_file_controller.validate_save_project('E:\Git\compilers_service\projects'); }).to.throw(CompilersServiceError, 'Project name in null or empty');
    });
    
    it("Send a language invvalid validate_save_file_data()", () => {
        let validation_file_controller = new ValidationProjectController();
        expect(() => { validation_file_controller.validate_save_project('E:\Git\compilers_service\projects', 'pruebaJava', 'c++'); }).to.throw(CompilersServiceError, 'this language does not exist');
    });
});
